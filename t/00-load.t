#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Myleen::Eiles' ) || print "Bail out!\n";
}

diag( "Testing Myleen::Eiles $Myleen::Eiles::VERSION, Perl $], $^X" );
